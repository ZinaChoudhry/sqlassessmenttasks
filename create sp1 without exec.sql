CREATE PROCEDURE sp1
@inputanydate varchar(20)

AS
declare @enddate varchar(20)
set @enddate = (select CAST(@inputanydate as numeric(20,0)) +500)

SELECT 
SUM([<vol>]*[<close>])/SUM([<vol>]) AS VWAP
, SUBSTRING(@inputanydate,11,2)+'/'+SUBSTRING(@inputanydate,9,2)+'/'+LEFT(@inputanydate,4) AS [Date]
, 'START ('+ SUBSTRING(@inputanydate,9,2)+':' +SUBSTRING(@inputanydate,11,2)+ ') - END ('+SUBSTRING(@enddate ,9,2)+':' +SUBSTRING(@inputanydate,11,2)+ ')' AS TimeInterval 
FROM
dbo.[sample_dataset2 (1)]
where 
[<date>] between @inputanydate and @enddate;


