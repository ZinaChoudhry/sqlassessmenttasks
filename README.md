﻿# SQL Challenges

## SQL Challenge 1

The aim of this task was to obtain the Volume weighted Average Price (VWAP) over a given time interval for any required date.

The Input:
The first part of this task required us to import a csv format table into SQL, using a Linux style scripting tool to obtain the specific precisions and scales for each of the table columns imported int SQL so that also values would be present upon execution. 
Where precision referred to the maximum total number of figures/characters whilst scale referred to the maximum possible number of digits after a decimal point for each corresponding column.

Execution:

In order to execute the commands upon creation of the stored procedure; the following was required:

EXEC[dbo].[sp1] '201010110900' whereby '201010110900' was an example of an INPUT datetime.

What this meant was that for any date provided in the format (YYYYMMDDHHMM) (YEARMONTHDAYHOURMINUTE) you were able to retrieve the results for the VWAP, the date on which 
it occured and then 5 hour time interval for this average, hence why the hour and minute were also provided within the input.

Assumptions:

The assumptions that were made during this task included:

Calculation of the VWAMP - For this a google search was required, and a chosen calculation procedure was assumed to be correct.

The trade price was also assumed to be the close price in the calculation of VWAP.

We also needed to take into consideration the times trading started and finished, because according to the data trading finished at a certain time so initially some results were returning time intervals which were less than five hours, we had to alter our statement in a way where the time interva; wasn't affected by the finish of trading times and would always return us a 5-hour interval.

## SQL Challenge 2

The assumptions we made in this challenge were how we wanted to calculate the range, I also used the 'abs' function which allowed all ranges to return a postive value.

The input in this challenge wasn't over a spread like challenge 1. This command ran in accordance with the inclusion of the necessary temporary tables, using join statements and dropping the temporary tables to output the three required columns.

Following on from this, it can be said that another assumption was the actual INPUT itself, where I had to determine the approrate creation of the temporary tables #maximumhigh and #timemaxhigh.

In order to execute the command following on from creation of the stored procedure, the following statement was executed: EXEC sp2
however, it must be noted that the  execution of this command must be done independently of the commands above it, so run separately in its own right.