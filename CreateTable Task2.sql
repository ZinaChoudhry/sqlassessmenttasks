USE [exampledb]
GO

/****** Object:  Table [dbo].[sample_dataset3]    Script Date: 9/18/2018 4:15:00 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[sample_dataset3](
	[Date] [varchar](10) NULL,
	[Time] [varchar](4) NULL,
	[Open] [numeric](6, 2) NULL,
	[High] [numeric](6, 3) NULL,
	[Low] [numeric](6, 3) NULL,
	[Close] [numeric](6, 4) NULL,
	[Volume] [int] NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


