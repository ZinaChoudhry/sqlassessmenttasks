create procedure sp2
AS
select top 3 [date],MAX(ABS([close]-[open])) AS TheRange 
INTO #TEMP1
FROM sample_dataset3
GROUP by [date]
ORDER BY THERANGE DESC

--SELECT THERANGE AS NewRange, [DATE] FROM #TEMP1 ORDER BY NewRange DESC

--select *
--from #TEMP1 
select
[date]
,max(high) as maximumhigh
INTO #maximumhigh
from [sample_dataset3]
where [date] IN (select [date] from #TEMP1)
GROUP by [date]

select distinct
d.date, d.time
INTO #timemaxhigh
from dbo.[sample_dataset3] d join #maximumhigh m on d.date=m.date and m.maximumhigh=d.high


select 
TR.date
,te.TheRange
,tr.time
FROM #TEMP1 te JOIN #timemaxhigh TR ON TR.DATE=Te.DATE

drop table 
#TEMP1

drop table #maximumhigh

drop table #timemaxhigh

EXEC sp2