USE [exampledb]
GO

/****** Object:  Table [dbo].[sample_dataset2 (1)]    Script Date: 9/18/2018 10:19:05 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[sample_dataset2 (1)](
	[<ticker>] [nvarchar](50) NULL,
	[<date>] [nvarchar](50) NULL,
	[<open>] [numeric](10, 6) NULL,
	[<high>] [numeric](10, 6) NULL,
	[<low>] [numeric](10, 6) NULL,
	[<close>] [numeric](10, 6) NULL,
	[<vol>] [int] NULL
) ON [PRIMARY]

GO


